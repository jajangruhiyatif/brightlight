+function (window, $, undefined) {

    'use strict';

/*
|--------------------------------------------------------------------------
| Global Brightligh Obj / Variables
|--------------------------------------------------------------------------
|
|
|
*/
  var BRIGHTLIGHT = window.BRIGHTLIGHT || {},
      $win = $(window);

/*
|--------------------------------------------------------------------------
| Js Class Detection
|--------------------------------------------------------------------------
|
|
|
*/

/*
|--------------------------------------------------------------------------
| Navigation Code
|--------------------------------------------------------------------------
|
|
|
*/
  BRIGHTLIGHT.Navigation = function () {
		$( '#dl-menu' ).dlmenu({
			animationClasses : { classin : 'dl-animate-in-5', classout : 'dl-animate-out-5' }
		});
  };

/*
|--------------------------------------------------------------------------
| Search Box Code
|--------------------------------------------------------------------------
|
|
|
*/

  BRIGHTLIGHT.searchInput = function () {
	new UISearch( document.getElementById( 'sb-search' ) );

	$( '.sb-search' ).on({
		click : function (e) {
				$(this).parent('.sb-container').addClass('sb-container-open');
		}
	});
  };
/*
|--------------------------------------------------------------------------
| LayerSlider Code
|--------------------------------------------------------------------------
|
|
|
*/

  BRIGHTLIGHT.LayerSlider = function ( elem, navBtn ) {
    var $elem = $( elem );
    if( $.fn.layerSlider && $elem ){
      $elem.layerSlider({
          skinsPath : 'layerslider/skins/',
          skin : 'fullwidth',
          thumbnailNavigation : 'disable',
          hoverPrevNext : false,
          responsive : true,
          responsiveUnder : 960,
          sublayerContainer : 900,
          navButtons:  ( navBtn === true ? true : false ),
          navPrevNext : false,
          navStartStop: false,
          showBarTimer: false,
          showCircleTimer: false
      });  
    }
  }; 

/*
|--------------------------------------------------------------------------
| Layout Builder
|--------------------------------------------------------------------------
|
|
|
*/

 BRIGHTLIGHT.portfolioCols = function (elem, container, filter, cols) {
    if( $.fn.isotope ) {


      $( elem ).each(function() {

        var $container = $( this ).find( container ),
            $filter = $( filter ),
            colsOptions = {};

          colsOptions = {
            resizesContainer: true
          };

        $container.imagesLoaded(function() {
          $container.isotope(colsOptions);
          });

		  var $titleContainer = $( '.title-container' );

		  if( $titleContainer.length > 0 ) {
			$( '.title-container' ).on( 'click', function() {
		  		$filter.stop(true,true).slideToggle(350, 'easeOutExpo', function(){
					var $this = $(this);

					if( $(this).hasClass('active-filter') ) {
						$this.css({'visibility': 'hidden', 'opacity': 0, 'display': 'inline-block'}).removeClass('active-filter');
						return;
					};
					$this.css({'visibility': 'visible', 'opacity': 1, 'display': 'inline-block'}).addClass('active-filter');
				});
		 	 });
		  };


          $filter.on( 'click', 'a', function( e ) {

            var $this = $(this);
            
            $container.isotope({
              filter: $this.data( 'filter' ),
              resizesContainer: true
            });

            $this.closest( 'li' ).addClass( 'active' ).siblings( 'li' ).removeClass( 'active' );

            $( '.title-container' ).text( $this.text() );

			if( $titleContainer.length > 0 ){
				$filter.slideUp(350, 'easeOutExpo', function(){
					$filter.css({'visibility': 'hidden', 'opacity': 0, 'display': 'inline-block'}).removeClass('active-filter');
				}); 
			}

            e.preventDefault();
          });


        });
	};
  };
/*
|--------------------------------------------------------------------------
| Layout Builder
|--------------------------------------------------------------------------
|
|
|
*/ 
  BRIGHTLIGHT.latestColumns = function (elem){
		var $elem = $( elem );
		if( $.fn.isotope && $elem.length > 0 ){
				var opt = {
						layoutMode: 'perfectMasonry',
						perfectMasonry: {
								layout: 'vertical',
								liquid: true,
								minCols: 1,              // Set min col count (default: 1)
								cols: 3                 // Force to have x columns (default: null)
								//rows: 2                 // Force to have y rows (default: null)
						}
				};
				$elem.isotope(opt);		
		};
		/*$elem.masonry({itemSelector: ".portfolio-items-container",columnWidth: function(e) {
            return e / 3
        },isResizable: true,isAnimated: !Modernizr.csstransitions,animationOptions: {duration: 250,easing: "swing"},isAnimatedFromBottom: true, isFitWidth: true }); */

		/*$elem.packery({
  itemSelector: '.portfolio-items-container',
  gutter: 0
});*/

  };


/*
|--------------------------------------------------------------------------
| FlexSliders Carousels
|--------------------------------------------------------------------------
|
|
|
*/

  BRIGHTLIGHT.Testimonials = function () {

    var Testimonials = $('#Testimonial-Carousel');

    if( Testimonials.length > 0 ){

      $win.on('load', function () {
        Testimonials.flexslider({
          animation: "slide",
          animationLoop: true,
          directionNav: false,
          itemWidth: 210,
          itemMargin: 5,
          minItems: 1,
          maxItems: 1
        });
      });
     
    }
    
  };

  BRIGHTLIGHT.Clients = function () {

    var Clients = $('#Clients-Carousel');

    if( Clients.length > 0 ){

      $win.on('load', function () {
        Clients.flexslider({
          animation: "slide",
          animationLoop: true,
          slideshow: false,
          directionNav: true,
          controlNav: false,
          itemWidth: 240,
          itemMargin: 5,
          minItems: 2,
          maxItems: 5,
          prevText: "<div class='rounded'><i class='icon-angle-left icon-large'></i></div>",
          nextText: "<div class='rounded'><i class='icon-angle-right icon-large'></i></div>",
          move: 1
        });
      });
      
    };
    
  };

  BRIGHTLIGHT.ArticleSlider = function () {

    var article = $('#article-slide');

    if( article.length > 0 ){

      $win.on('load', function () {
        article.flexslider({
          animation: "slide",
          animationLoop: true,
          slideshow: true,
          directionNav: false,
          controlNav: true
        });
      });
      
    };
    
  };

/*
|--------------------------------------------------------------------------
| Flickr 
|--------------------------------------------------------------------------
|
|
|
*/
  BRIGHTLIGHT.Flickr = function () {
    var $flickr = $('.flickr-imgs');
    if( $flickr.length > 0 ){
       $flickr.jflickrfeed({
          limit: 12,
          qstrings: {
              id: '97861178@N08'
          },

          useTemplate: false,
          itemCallback: function(item){
            $(this).append(
              "<li>" + 
                  '<a href="' + item.image_b + '">' +
                    '<img src="' + item.image_s + '" alt="' + item.title + '"' + 'class="img-responsive"' + '>' +
                  '</a>' +
              '</li>'
            );
          }
      });
    };
  }

/*
|--------------------------------------------------------------------------
| Instagram Feed 
|--------------------------------------------------------------------------
|
|
|
*/

BRIGHTLIGHT.instagramFeed = function () {
		var $insta = $('#instafeed');
		if ( $insta.length > 0) {
				  var userFeed = new Instafeed({
						get: 'user',
						userId: 317769648,
						accessToken: '317769648.8192986.3bbab2a88a9642358d81b860150da43e',
						template: '<li><a class="animation" href="{{link}}"><img src="{{image}}" alt="img" /></a></li>',
						limit : 9
					});
				  userFeed.run();
		};
};

/*
|--------------------------------------------------------------------------
| Brightlight Gallery Carousel
|--------------------------------------------------------------------------
|
|
|
*/

BRIGHTLIGHT.galleryCarousel = function () {
		var elem = $('#portfolio-gallery');
		if( $.fn.swiper && elem.length > 0){
			var gallery = elem.swiper({
				slidesPerView:'auto',
				watchActiveIndex: true,
				centeredSlides: true,
				pagination:'.pagination',
				paginationClickable: true,
				resizeReInit: true,
				keyboardControl: true,
				grabCursor: true,
				onImagesReady: function(){
					changeSize()
				}
			});
			$('.previous-swiper').on({ click : function () {
					gallery.swipePrev();	
				}	
			});
			$('.next-swiper').on({
				click: function () {
					gallery.swipeNext()	
				}
			});
			var changeSize = function () {
				//Unset Width
				$('.swiper-slide').css('width','')
				//Get Size
				var imgWidth = $('.swiper-slide img').width();
				if (imgWidth+40>$(window).width()) imgWidth = $(window).width()-40;
				//Set Width
				$('.swiper-slide').css('width', imgWidth+40);
			}
			
			changeSize();

			//Smart resize
			$(window).resize(function(){
				changeSize()
				gallery.resizeFix(true)	
			});
				
		}
};
BRIGHTLIGHT.teamCarousel = function () {
	var elem = $('#about-carousel');
	if( $.fn.swiper && elem.length > 0 ) {
		  var mySwiper = elem.swiper({
			slidesPerView: 'auto',
			loop: false 
		    
		  });

			$('.swiper-about-next a').on({ 
				click : function (e) {
					mySwiper.swipePrev();	
					e.preventDefault();
				}	
			});
			$('.swiper-about-prev a').on({
				click: function (e) {
					mySwiper.swipeNext();
					e.preventDefault();
				}
			});

	}
};

/*
|--------------------------------------------------------------------------
| Popup Controller 
|--------------------------------------------------------------------------
|
|
|
*/

BRIGHTLIGHT.popup = function ( elem ) {
		var $elem = $( elem );
		if( $elem.length > 0 && $.fn.magnificPopup ) {
				$elem.magnificPopup({
						type: 'image',
						removalDelay: 500, //delay removal by X to allow out-animation
						  callbacks: {
							beforeOpen: function() {
							  // just a hack that adds mfp-anim class to markup 
							   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
							   this.st.mainClass = this.st.el.attr('data-effect');
							}
						  },
						  closeOnContentClick: true,
						  midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.

				});
		};
};

/*
|--------------------------------------------------------------------------
| Video Popup 
|--------------------------------------------------------------------------
|
|
|
*/ 

BRIGHTLIGHT.videoPopUp = function ( elem ) {
		var $elem = $( elem );
		if( $elem.length > 0 && $.fn.magnificPopup ) {
				$elem.magnificPopup({
						delegate : 'a',
						type : 'iframe',
						removalDelay: 500, //delay removal by X to allow out-animation
						  callbacks: {
							beforeOpen: function() {
							  // just a hack that adds mfp-anim class to markup 
							   this.st.image.markup = this.st.image.markup.replace('mfp-figure', 'mfp-figure mfp-with-anim');
							   this.st.mainClass = this.st.el.attr('data-effect');
							}
						  },
						  closeOnContentClick: true,
						 preloader: false,
						  midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.

				});
		};
};


/*
|--------------------------------------------------------------------------
| Functions API Initializers
|--------------------------------------------------------------------------
|
|
|
*/

  BRIGHTLIGHT.Navigation();
  BRIGHTLIGHT.searchInput();
  BRIGHTLIGHT.LayerSlider('#layerslider', true);
  BRIGHTLIGHT.LayerSlider('#layerslider2');
  BRIGHTLIGHT.latestColumns( '.portfolio-items' );
  BRIGHTLIGHT.portfolioCols('.portfolio-isotope-4c','#portfolio-container', '#filter-list');
  BRIGHTLIGHT.Testimonials();
  BRIGHTLIGHT.Clients();
  BRIGHTLIGHT.ArticleSlider();
  BRIGHTLIGHT.Flickr();
  BRIGHTLIGHT.instagramFeed();
  BRIGHTLIGHT.galleryCarousel();
  BRIGHTLIGHT.teamCarousel();
  BRIGHTLIGHT.popup('.popup-link');
  BRIGHTLIGHT.videoPopUp('.vid-container');
  //EXPOSE THE OBJ TO THE GLOBAL WINDOW

  window.BRIGHTLIGHT = BRIGHTLIGHT;
  
} (window, window.jQuery, undefined);
