<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
		$this->load->model('M_home');
		// $this->load->model('M_tour');
		$this->load->model('M_tour');
		$this->load->model('M_rent');
		$this->load->model('M_gallery');
		$this->load->model('M_about');
		$this->load->model('M_testimony');
	}
	
	public function login()
	{
		$this->load->view('frontend/_page/login');
	}
	
	public function index()
	{
		$data['about'] = $this->M_about->list_data();
		$data['activity'] = $this->M_tour->get_activity();
		$data['testimony'] = $this->M_testimony->list_data();
		$data['data'] = $this->M_home->list_data();
		$this->load->view('frontend/_page/index', $data);
	}
	

	public function gallery()
	{
		$data['about'] = $this->M_about->list_data();
		$data['gallery'] = $this->M_gallery->list_data();
		$data['activity'] = $this->M_tour->get_activity();
		$this->load->view('frontend/_page/gallery', $data);
	}

	public function price_list()
	{
		$data['about'] = $this->M_about->list_data();
		$data['gallery'] = $this->M_gallery->list_data();
		$data['rent'] = $this->M_rent->list_data();
		$data['activity'] = $this->M_tour->get_activity();
		$this->load->view('frontend/_page/price_list', $data);
	}

	public function tour_package()
	{
		$data['about'] = $this->M_about->list_data();
		$data['activity'] = $this->M_tour->get_activity();
		$data['data'] = $this->M_tour->list_data();
		$this->load->view('frontend/_page/tour_package', $data);
	}
	
	public function about_us()
	{
		$data['about'] = $this->M_about->list_data();
		$data['gallery'] = $this->M_gallery->list_data();
		$data['activity'] = $this->M_tour->get_activity();
		$data['testimony'] = $this->M_testimony->list_data();
		$this->load->view('frontend/_page/about', $data);
	}

	public function contact()
	{
		$data['about'] = $this->M_about->list_data();
		$data['activity'] = $this->M_tour->get_activity();
		$this->load->view('frontend/_page/contact', $data);
	}

}
