<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Testimony extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('M_testimony');
        if($this->session->userdata('status') != 'Login') {
          redirect(base_url('credential/login'));
        }
      }

      public function index()
      {
        // $data['data'] = $this->m_home->list_data();
        $this->load->view('backend/page/testimony');
      }

      public function get_data()
      {
        $data = $this->M_testimony->list_data();
        echo json_encode($data);
      }

      public function add_data()
      {
        $data = array();
        $msg = ""; $status;
        $content = $this->input->post('content');
        $sub_content = $this->input->post('sub_content');
        $created_by = $this->input->post('created_by');
        
        $data = array(
          'created_by' => $created_by,
          'content' => $content,
          'sub_content' => $sub_content
        );

        $insert = $this->M_testimony->insert_data($data);
        
        if($insert) {
          $status = 200;
          $msg = "Content has been added successfully.";
        } else {
          $status = 302;
          $msg = "Failed to add content, try again.";
        }
        
        echo json_encode(array('msg' => $msg, 'status' => $status));

      }

      public function edit_data()
      {
        
      }

      public function delete_data()
      {
        $msg = ""; $status;
        $id = $this->input->post('content_id');

        $delete = $this->M_testimony->destroy($id);
        if($delete) {
          $status = 200;
          $msg = "Data has been deleted.";
        } else {
          $status = 302;
          $msg = "Something went wrong.";
        }

        $response = array(
          'status' => $status,
          'msg' => $msg
        );

        echo json_encode($response);

      }

  }

?>