<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Home extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('M_home');

        if($this->session->userdata('status') != 'Login') {
          redirect(base_url('credential/login'));
        }
        
      }

      public function index()
      {
        $this->load->view('backend/page/home');
      }

      public function get_data()
      {
        $data = $this->M_home->list_data();
        echo json_encode($data);
      }

      public function add_data()
      {
        $data = array();
        $msg = ""; $status;
        $content_1 = $this->input->post('content_1');
        $content_2 = $this->input->post('content_2');
        $content_3 = $this->input->post('content_3');
        $type = $this->input->post('type');
        
        $data = array(
          'content_1' => $content_1,
          'content_2' => $content_2,
          'content_3' => $content_3,
          'type' => $type
        );

        $insert = $this->M_home->insert_data($data);
        
        if($insert) {
          $status = 200;
          $msg = "Content has been added successfully.";
        } else {
          $status = 302;
          $msg = "Failed to add content, try again.";
        }

        $response = array(
          'status' => 200,
          'msg' => $msg
        );
        
        echo json_encode($response);

      }

      public function edit_data()
      {
        
      }

      public function delete_data()
      {
        $msg = ""; $status;
        $id = $this->input->post('content_id');

        $delete = $this->M_home->destroy($id);
        if($delete) {
          $status = 200;
          $msg = "Data has been deleted.";
        } else {
          $status = 302;
          $msg = "Something went wrong.";
        }

        $response = array(
          'status' => $status,
          'msg' => $msg
        );

        echo json_encode($response);

      }
      
  }

?>