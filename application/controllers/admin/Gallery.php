<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Gallery extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('M_gallery');
        if($this->session->userdata('status') != 'Login') {
          redirect(base_url('credential/login'));
        }
      }

      public function index()
      {
        $this->load->view('backend/page/gallery');
      }

      public function get_data()
      {
        $data = $this->M_gallery->list_data();
        echo json_encode($data);
      }

      public function add_data()
      {
        $msg  = ""; $status = "";
        $file = $_FILES['image'];
        $target_dir  = 'assets/upload/gallery/';
        $target_file = $target_dir . basename($_FILES['image']['name']);
        
        if(move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
          $insert = $this->M_gallery->insert_data($target_file);

          if($insert) {
            $msg = 'Data hasbeen saved';
            $status = 200;
          } else {
            $msg = 'Something went wrong';
            $status = 302;
          }
        } else {
          $msg = 'Cant move image data';
          $status = 500;
        }        
        echo json_encode(array('status' => $status, 'msg' => $msg));
      }

      public function delete_data()
      {
        $msg = ""; $status;
        $id = $this->input->post('gallery_id');

        $delete = $this->M_gallery->destroy($id);
        if($delete) {
          $status = 200;
          $msg = "Data has been deleted.";
        } else {
          $status = 302;
          $msg = "Something went wrong.";
        }

        $response = array(
          'status' => $status,
          'msg' => $msg
        );

        echo json_encode($id);
      }

  }

?>