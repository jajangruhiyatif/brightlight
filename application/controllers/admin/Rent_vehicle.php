<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class rent_vehicle extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('M_rent');
        if($this->session->userdata('status') != 'Login') {
          redirect(base_url('credential/login'));
        }
      }

      public function index()
      {
        $this->load->view('backend/page/rent');
      }

      public function get_data()
      {
        $data = $this->M_rent->list_data();
        echo json_encode($data);
      }

      public function add_data()
      {
        $msg = ""; $status = "";
        
        $price    = $this->input->post('price');
        $type     = $this->input->post('type');
        $brand    = $this->input->post('brand');
        $duration = $this->input->post('duration');
        $region   = $this->input->post('region');
        
        $data = array(
          'price' => $price,
          'vehicle_type' => $type,
          'vehicle_brand' => $brand,
          'duration' => $duration,
          'region' => $region,
        );

        $insert = $this->M_rent->insert_data($data);

        if ($insert) {
          $msg = "Data has been added. successfully.";
          $status = 200;
        } else {
          $msg = "Somethng went wrong.";
          $status = 302;
        }

        echo json_encode(array('status' => $status, 'msg' => $msg));
      }

      
      public function delete_data()
      {
        $msg = ""; $status;
        $id = $this->input->post('content_id');

        $delete = $this->M_rent->destroy($id);
        if($delete) {
          $status = 200;
          $msg = "Data has been deleted.";
        } else {
          $status = 302;
          $msg = "Something went wrong.";
        }

        $response = array(
          'status' => $status,
          'msg' => $msg
        );

        echo json_encode($response);

      }

  }

?>