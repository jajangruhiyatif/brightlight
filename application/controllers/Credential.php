<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Credential extends CI_Controller
  {
      public function __construct()
      {
        parent::__construct();
        $this->load->model('M_user');
        $this->load->helper('url');
      }
      
      public function Login()
      {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $where = array(
          'username' => $username,
          'password' => md5($password)
          );
        $check = $this->M_user->validate('user', $where)->num_rows();

        if($check > 0) {
          $data_session = array(
            'name' => $username,
            'status' => 'Login'
            );

          $this->session->set_userdata($data_session);
          
          print ("ok login");
          // $this->load->view('backend/index');
          redirect(base_url('home'));
        } else {
          echo "Invalid credential";
          redirect(base_url('login'));
        }
      }

      public function Logout()
      {
        $this->session->sess_destroy();
        redirect(base_url(''));
      }
  }
?>