<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class M_tour extends CI_Model
  {
    private $table = 'master_tour';

    function list_data()
    {
      $query = $this->db->get($this->table);
      return $query->result();
    }

    function get_activity()
    {
      $query = $this->db->get($this->table, 3);
      return $query->result();
    }

    function insert_data($image)
    {
      $post = $this->input->post();
      
      $data = array(
        'title' => $post['title'],
        'price' => $post['price'],
        'description' => $post['description'],        
        'image_path'  => $image,
        'created_date' => date('d-m-Y'),
      );
      
      $this->db->insert($this->table, $data);
    }

    function destroy($id)
    {
      $delete = $this->db->delete($this->table, array('id' => $id));
      return $delete;
    }
    
    private function _uploadImage()
    {
      $file = $_FILES['image'];
      $target_dir = 'assets/upload/tour/';
      $target_file = $target_dir . basename($_FILES['image']['name']);
      if(move_uploaded_file($_FILES['image']['tmp_name'], $target_file)) {
        echo 'ok';
      } else {
        echo 'not ok';
      }
    }

  }

?>