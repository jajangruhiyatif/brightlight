<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class M_about extends CI_Model
  {
    private $table = 'master_about';

    function list_data()
    {
      $this->db->order_by('type', 'ASC');
      $query = $this->db->get($this->table);
      return $query->result();
    }

    function insert_data($data = array())
    {
      $query = $this->db->insert($this->table, $data);
      return $query;
    }

    function destroy($id)
    {
      $delete = $this->db->delete($this->table, array('id' => $id));
      return $delete;
    }

  }

?>