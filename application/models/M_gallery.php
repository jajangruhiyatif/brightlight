<?php defined('BASEPATH') OR exit('No direct script access allowed');

  class M_gallery extends CI_Model
  {
    private $table = 'master_gallery';

    function list_data()
    {
      $query = $this->db->get($this->table);
      return $query->result();
    }

    function insert_data($image)
    {
      $post = $this->input->post();
      
      $data = array(
        'title' => $post['title'],
        'type'  => $post['type'],
        'image_path'  => $image,
      );
      
      $this->db->insert($this->table, $data);
    }

    function destroy($id)
    {
      $delete = $this->db->delete($this->table, array('id' => $id));
      return $delete;
    }
    
  }

?>