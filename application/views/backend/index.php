<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Content</title>
  <?php $this->load->view('backend/partial/_header'); ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('backend/partial/_navbar');?>
  <?php $this->load->view('backend/partial/_sidebar'); ?>
  <?php $this->load->view('backend/partial/_body'); ?>

  <?php //$this->load->view('backend/page/index'); ?>
   
  <?php $this->load->view('backend/partial/_footer'); ?>
</body>
</html>