<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Master Content</title>
  <?php $this->load->view('backend/partial/_header'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('backend/partial/_navbar');?>
  <?php $this->load->view('backend/partial/_sidebar'); ?>
  <?php $this->load->view('backend/partial/_body'); ?>

    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Testimony</h3>

      </div>
      <div class="box-body table-responsive pad">
        
        <table class="table table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th>Content</th>
            <th>Sub content</th>
            <th width="13%">Created By</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody id="content-data">

          </tbody>
        </table>
      </div>

      <!-- begin modal  -->
      <div class="modal fade" id="modal-create">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Master Testimony | Create</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Content *</label>
                <input type="text" class="form-control" id="content" placeholder=""><br>
                <label>Sub content</label>
                <input type="text" class="form-control" id="sub_content" placeholder=""><br>
                <label>Created By</label>
                <input type="text" class="form-control" id="created_by" placeholder=""><br>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-save" data-dismiss="modal">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Master Testimony | Edit</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Content 1 *</label>
                <input type="text" class="form-control" id="content_1" placeholder=""><br>
                <label>Content 2</label>
                <input type="text" class="form-control" id="content_2" placeholder=""><br>
                <textarea></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-warning btn-edit" data-dismiss="modal">update change</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-delete" abindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
              <div>
              <label>Are you sure want to delete content. ?</label>                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-delete" data-dismiss="modal">Yes</button>
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end of modal -->

      <div class="box-footer clearfix">
        <button class="btn btn-sm btn-info btn-flat" data-toggle="modal" data-target="#modal-create">Create new</button>
      </div>
    </div>

  <?php $this->load->view('backend/partial/_footer'); ?>

<script>
  $(document).ready(function(){
    show_data();

     
    $("#modal-create").on('shown.bs.modal', function(){
        $(this).find('input[type="number"]').focus();
    });

    $("#modal-create").on("hidden.bs.modal", function(){
      $('#content').val("");
      $('#sub_content').val("");
      $('#created_by').val("");
    });

    // get list content
    function show_data() {
      $.ajax({
        type: 'GET',
        url: '<?php echo base_url('testimony/get_data') ?>',
        async: true,
        dataType: 'json',
        success: function(data) {
          var html = ""; var no = 1;
          for (var i=0; i<data.length; i++ ) {
            html += 
              "<tr>\
                <td>" + (no+i) + "</td>\
                <td>" + data[i].content + "</td>\
                <td>" + data[i].sub_content + "</td>\
                <td>" + data[i].created_by + "</td>\
                <td>\
                <a href=\"#\" data-target=\"#modal-edit\" data-toggle=\"modal\" \
                    data-content-id=" + data[i].id      + "\
                    data-content="    + data[i].content + "\
                    data-subcontent=" + data[i].sub_content + "\
                    data-created-by=" + data[i].created_by + ">\
                    <i class=\"fa fa-fw fa-edit\"></i>\
                  </a>\
                  <a href=\"#\" data-target=\"#modal-delete\" data-toggle=\"modal\" data-content-id=" + data[i].id + ">\
                    <i class=\"fa fa-fw fa-remove\"></i>\
                  </a>\
                </td>\
              </tr>"
          }
          $('#content-data').html(html);
        },
        error: function(data, error) {
          console.log(error);
        },
        complete: function(data) {
          
        }
      });
    }
    
    // save
    $('.btn-save').on('click', function(e) {
      var button = $(e.relatedTarget);
      var content = $('#content').val();
      var sub_content = $('#sub_content').val();
      var created_by = $('#created_by').val();

      $.ajax({
        type: 'POST',
        url: '<?php echo base_url('testimony/add_data') ?>',
        dataType: 'JSON',
        data: { 
          content: content, 
          sub_content: sub_content,           
          created_by: created_by,
        },
        success: function(data) {
          show_data();
        },
        error: function(data, error){
          console.log('err insert');
        }
      });

    });

    $('#modal-edit').on('show.bs.modal', function(e) {
      var button   = $(e.relatedTarget);
      var title_content = button.data('title-content');

      var modal = $(this)
      modal.find('.modal-body input').val(title_content);

      $('.btn-edit').on('click', function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('admin/title/test') ?>',
          data: { id: title_id },
          success: function() {
            show_data();
          },
        })
      });
    });

    // delete
    $('#modal-delete').on('show.bs.modal', function(e) {
      var button   = $(e.relatedTarget)
      var content_id = button.data('content-id');

      $('.btn-delete').on('click', function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('testimony/delete_data') ?>',
          data: { content_id: content_id },
          success: function(data) {
            show_data();
          },
        })
      });
    });

  });
</script>
</body>
</html>
