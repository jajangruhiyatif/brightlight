<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Master Content</title>
  <?php $this->load->view('backend/partial/_header'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('backend/partial/_navbar');?>
  <?php $this->load->view('backend/partial/_sidebar'); ?>
  <?php $this->load->view('backend/partial/_body'); ?>

    <div class="box">
      <div class="box-header">
        <h3 class="box-title">About</h3>

      </div>
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th width="10%;">Title</th>
            <th width="70%">Content</th>
            <th>Type</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody id="content-data">
            
          </tbody>
        </table>
      </div>

      <!-- begin modal  -->
      <div class="modal fade" id="modal-create">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Master Home | Create</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Content 1 *</label>
                <input type="text" class="form-control" id="content_1" placeholder="" maxlength="50"><br>
                <label>Content 2</label>
                <textarea class="textarea" id="content_2"
                          placeholder="Place some text here, if the content has more length"
                          style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                </textarea>
                <label>Type</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected" value="0">Pos 1</option>
                  <option value="1">Pos 2</option>
                  <option value="2">Pos 3</option>
                  <option value="3">Body Content</option>
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary btn-save" data-dismiss="modal">Save changes</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Master Home | Edit</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Content 1 *</label>
                <input type="text" class="form-control" id="content_1" placeholder="" maxlength="50"><br>
                <label>Content 2</label>
                <input type="text" class="form-control" id="content_2" placeholder="" maxlength="50"><br>
                <label>Content 3</label>
                <!-- <input type="text" class="form-control" id="content_3" placeholder=""><br> -->
                <div class="box-body pad">
                  <form>
                    <textarea class="textarea" id="content_3"
                              placeholder="Place some text here, if the content has more length"
                              style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">
                    </textarea>
                  </form>
                </div>
                <label>Type</label>
                <select class="form-control select2" style="width: 100%;">
                  <option selected="selected" value="0">Banner</option>
                  <option value="1">Body 1</option>
                  <option value="2">Body 2</option>
                  <option value="3">Body 3</option>
                  <option value="4">Info price</option>
                  <option value="5">Info Package</option>
                </select>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-warning btn-edit" data-dismiss="modal">update change</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-delete" abindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
              <div>
              <label>Are you sure want to delete content. ?</label>                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-delete" data-dismiss="modal">Yes</button>
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end of modal -->

      <div class="box-footer clearfix">
        <button class="btn btn-sm btn-info btn-flat" data-toggle="modal" data-target="#modal-create">Create new</button>
      </div>
    </div>

  <?php $this->load->view('backend/partial/_footer'); ?>

<script>
  $(document).ready(function(){
    
    $("#modal-create").on('shown.bs.modal', function(){
        $(this).find('input[type="text"]').focus();
    });

    $("#modal-create").on("hidden.bs.modal", function(){
      $("#content_1").val("");
      $("#content_2").val("");
    });
    
    show_data();

    // get list content
    function show_data() {
      $.ajax({
        type: 'GET',
        url: '<?php echo base_url('about/get_data') ?>',
        async: true,
        dataType: 'json',
        success: function(data) {
          var html = ""; var no = 1; var vadage = "";
          for (var i = 0; i < data.length; i++) {
            if(data[i].type == 0){
              badage = "About 1"
            } else if(data[i].type == 1) {
              badage = "About 2"
            } else if(data[i].type == 2) {
              badage = "About 3"
            } else {
              badage = "Body Content"
            }
            html += 
            "<tr>\
            <td>" + (no+i) + "</td>\
                <td>" + data[i].content_1 + "</td>\
                <td>" + data[i].content_2 + "</td>\
                <td><span class=\"label label-primary\">" + badage + "</span></td>\
                <td>\
                <a href=\"#\" data-target=\"#modal-edit\" data-toggle=\"modal\" \
                data-content_id=" + data[i].id + " \
                   data-content1=" + data[i].content_1 + " \
                   data-content2=" + data[i].content_2 + " \
                   data-type=" + data[i].type + ">\
                   <i class=\"fa fa-fw fa-edit\"></i>\
                  </a>\
                  <a href=\"#\" data-target=\"#modal-delete\" data-toggle=\"modal\" data-content-id=" + data[i].id + ">\
                  <i class=\"fa fa-fw fa-remove\"></i>\
                  </a>\
                </td>\
                </tr>"
            $('#content-data').html(html);
          }
        },
        error: function(data, error) {

        },
        complete: function(data) {
          
        }
      });
    }
    
    // save
    $('.btn-save').on('click', function(e) {
      var button = $(e.relatedTarget);
      var content_1 = $('#content_1').val();
      var content_2 = $('#content_2').val();
      var type = $('.select2').find("option:selected").val();

      $.ajax({
        type: 'POST',
        url: '<?php echo base_url('about/add_data') ?>',
        dataType: 'JSON',
        data: { 
          content_1: content_1, 
          content_2: content_2, 
          type: type,
        },
        success: function(data) {
          show_data();
        },
        error: function(data, error){
          console.log('err insert');
        }
      });
    });

    // edit
    $('#modal-edit').on('show.bs.modal', function(e) {
      var button   = $(e.relatedTarget)
      var content_id = button.data('content_id')
      var content_1 = button.data('content1')
      var content_2 = button.data('content2')
      var content_3 = button.data('content3')
      var type = button.data('type')

      var modal = $(this)
      
      // modal.find('#content_1').val(content_1);
      // modal.find('#content_2').val(content_2);
      // modal.find('#content_3').val(content_3);

      console.log(content_1);
      // $('.btn-edit').on('click', function() {
      //   $.ajax({
      //     type: 'POST',
      //     url: '<?php echo base_url('admin/asd') ?>',
      //     data: { id: title_id },
      //     success: function() {
      //       show_data();
      //     },
      //   })
      // });
    });

    // delete
    $('#modal-delete').on('show.bs.modal', function(e) {
      var button   = $(e.relatedTarget)
      var content_id = button.data('content-id');

      $('.btn-delete').on('click', function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('about/delete_data') ?>',
          data: { id: content_id },
          success: function(data) {
            show_data();
          },
        })
      });
    });

  });
</script>
</body>
</html>
