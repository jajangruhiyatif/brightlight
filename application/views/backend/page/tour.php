<!DOCTYPE html>
<html>
<head>
  <title>Administrator | Master Content</title>
  <?php $this->load->view('backend/partial/_header'); ?>

</head>
<body class="hold-transition skin-blue sidebar-mini">
  <?php $this->load->view('backend/partial/_navbar');?>
  <?php $this->load->view('backend/partial/_sidebar'); ?>
  <?php $this->load->view('backend/partial/_body'); ?>

    <div class="box">
      <div class="box-header">
        <h3 class="box-title">Testimony</h3>

      </div>
      <div class="box-body table-responsive pad">
        
        <table class="table table-hover">
          <thead>
          <tr>
            <th>No</th>
            <th>Picture</th>
            <th width="7%">Price</th>
            <th width="7%">Title</th>
            <th>Description</th>            
            <th width="7%">Created Date</th>
            <th width="7%">Update Date</th>
            <th>Action</th>
          </tr>
          </thead>
          <tbody id="content-data">
          
          </tbody>
        </table>
      </div>

      <!-- begin modal  -->
      <div class="modal fade" id="modal-create">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Master Tour | Create</h4>
            </div>
            
            <form class="form-horizontal" method="post" action="" id="upload_form" enctype="multipart/form-data" >
              <div class="modal-body">
                <div>
                  <label for="title">Title</label>
                  <input type="text" class="form-control" id="title" placeholder=""><br>
                  <label for="price">Price</label>
                  <input type="number" class="form-control" id="price" placeholder=""><br>
                  <label for="description">Description</label>
                  <textarea class="textarea" id="description"
                            placeholder=""
                            style="width: 100%; height: 150px; font-size: 14px; ">
                  </textarea>
                  <label for="image">File input</label>
                  <div class="row form-group">
                    <div class="col-md-4">
                      <img id="blah" src="<?php echo base_url("assets/upload/noimage.png") ?>" width='50' height='50' alt="your image" /><br/><br/>
                      <input type="file" name="image_path" id="image_path" multiple="true" accept="image/*" onchange="readURL(this);">
                    </div>
                  </div>

                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary btn-save" data-dismiss="modal">Save changes</button>
                <!-- <input type="submit" value="submit" class="btn btn-primary btn-save" > -->
              </div>
            </form>

          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-edit">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Master Home | Edit</h4>
            </div>
            <div class="modal-body">
              <div>
                <label>Content 1 *</label>
                <input type="text" class="form-control" id="content_1" placeholder=""><br>
                <label>Content 2</label>
                <input type="text" class="form-control" id="content_2" placeholder=""><br>
                <textarea></textarea>
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-warning btn-edit" data-dismiss="modal">update change</button>
            </div>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-delete" abindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title">Delete</h4>
            </div>
            <div class="modal-body">
              <div>
              <label>Are you sure want to delete content. ?</label>                
              </div>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-danger btn-delete" data-dismiss="modal">Yes</button>
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal">No</button>
            </div>
          </div>
        </div>
      </div>
      <!-- end of modal -->

      <div class="box-footer clearfix">
        <button class="btn btn-sm btn-info btn-flat" data-toggle="modal" data-target="#modal-create">Create new</button>
      </div>
    </div>

  <?php $this->load->view('backend/partial/_footer'); ?>

<script type="text/javascript">

  function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
  }

  $(document).ready(function(e){
    show_data();

    $("#modal-create").on('shown.bs.modal', function(){
        $(this).find('input[type="text"]').focus();
    });

    $("#modal-create").on("hidden.bs.modal", function(){
      $("#title").val("");
      $("#title").val("");
      $("#description").val("");
      $("#image_path").val("");
    });

    // get list content
    function show_data() {
      var url_data = "http://127.0.0.1/brightlight/"
      $.ajax({
        type: 'GET',
        url: '<?php echo base_url('tour_package/get_data') ?>',
        async: true,
        dataType: 'json',
        success: function(data) {
          var html = ""; var no = 1; var update = "-";
          for (i = 0; i < data.length; i++ ) {
            if(data[i].updated_date == null) {
              data[i].updated_date = update;
            } 
            html += 
            "<tr>\
              <td>" + (no+i) + "</td>\
              <td><img src=" + url_data + data[i].image_path + " width=\'75\' height=\'75\'></td>\
              <td>Rp. " + data[i].price + "</td>\
              <td>" + data[i].title + "</td>\
              <td>" + data[i].description + "</td>\
              <td>" + data[i].created_date + "</td>\
              <td>" + data[i].updated_date + "</td>\
              <td>\
              <a href=\"#\" data-target=\"#modal-edit\" data-toggle=\"modal\" \
                   data-tour_id=" + data[i].id + " \
                   data-price="   + data[i].price + " \
                   data-desc="    + data[i].description + " \
                   data-created=" + data[i].created_date + " \
                   data-update="  + data[i].updated_date + ">\
                    <i class=\"fa fa-fw fa-edit\"></i>\
                  </a>\
                  <a href=\"#\" data-target=\"#modal-delete\" data-toggle=\"modal\" data-tour-id=" + data[i].id + ">\
                    <i class=\"fa fa-fw fa-remove\"></i>\
                  </a>\
              </td>\
            </tr>";
            $('#content-data').html(html);
          }

        },
        error: function(data, error) {
          console.log(error);
        },
        complete: function(data) {
          
        }
      });
    }
    
    $("#modal-create").on('shown.bs.modal', function(){
      $(this).find('input[type="text"]').focus();
    });

    // save
    $('.btn-save').on('click',function(e) {
      e.preventDefault();
      var title = $('#title').val();
      var price = $('#price').val();
      var desc = $('#description').val();
      
      var data = new FormData();

      $.each($('#image_path')[0].files, function(i, file){
        data.append('image', file);
        data.append('description', desc);
        data.append('title', title);
        data.append('price', price);
      });

      $.ajax({
        url: "<?php echo base_url('tour_package/add_data') ?>",
        type: "POST",
        dataType: "JSON",
        contentType: false,
        processData: false,
        data: data,
        success: function (data) {
          show_data();
        },
        error: function(data, error) {
          console.log('err');
        }
      });
    });

    $('#modal-edit').on('show.bs.modal', function(e) {
      var button   = $(e.relatedTarget);
      var title_content = button.data('title-content');

      var modal = $(this)
      modal.find('.modal-body input').val(title_content);

      $('.btn-edit').on('click', function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('admin/title/test') ?>',
          data: { id: title_id },
          success: function() {
            show_data();
          },
        })
      });
    });

    // delete
    $('#modal-delete').on('show.bs.modal', function(e) {
      var button = $(e.relatedTarget)
      var id = button.data('tour-id');

      $('.btn-delete').on('click', function() {
        $.ajax({
          type: 'POST',
          url: '<?php echo base_url('tour_package/delete_data') ?>',
          data: { tour_id: id },
          success: function(data) {
            show_data();
          },
        })
      });
    });

  });
</script>
</body>
</html>
