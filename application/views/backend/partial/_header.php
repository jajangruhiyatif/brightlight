<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

<title>Admin Page</title>

<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/bower_components/bootstrap/dist/css/bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/bower_components/font-awesome/css/font-awesome.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/bower_components/Ionicons/css/ionicons.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/dist/css/AdminLTE.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/dist/css/skins/_all-skins.min.css') ?>">
<link rel="stylesheet" href="<?php echo base_url('assets/adminLTE/plugins/iCheck/square/blue.css') ?>">
