  <aside class="main-sidebar">
    
    <section class="sidebar">
      
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/adminLTE/dist/img/user2-160x160.jpg') ?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Alexander Pierce</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
            <button type="submit" name="search" id="search-btn" class="btn btn-flat">
              <i class="fa fa-search"></i>
            </button>
          </span>
        </div>
      </form>

      <ul class="sidebar-menu" data-widget="tree">
           
        <li class="treeview">
          <a href="#">
            <i class="fa fa-edit"></i> <span>Master Content</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('home'); ?>"><i class="fa fa-circle-o"></i> Home</a></li>
            <li><a href="<?php echo base_url('testimony/index'); ?>"><i class="fa fa-circle-o"></i> Testimony</a></li>
            <li><a href="<?php echo base_url('gallery/index'); ?>"><i class="fa fa-circle-o"></i> Gallery</a></li>
            <li><a href="<?php echo base_url('about/index'); ?>"><i class="fa fa-circle-o"></i> About</a></li>
          </ul>
        </li>
         
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Master Package</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url('tour_package/index'); ?>"><i class="fa fa-circle-o"></i>Tour Package</a></li>
            <li><a href="<?php echo base_url('rent_vehicle/index'); ?>"><i class="fa fa-circle-o"></i>Vehicle Rent</a></li>
          </ul>
        </li>
        
    </section>
  </aside>


  