<?php $this->load->helper("url"); ?>
<?php $this->load->view('frontend/_partial/header'); ?>

<link rel="stylesheet" href="<?php echo base_url('assets/css/gallery-app.css') ?>">

<?php $this->load->view('frontend/_partial/navbar'); ?>

	<div class="swiper-container" id="portfolio-gallery"> 
		<div class="previous-swiper"><i class="icon-arrow-left7 icon-large"></i></div>
		<div class="next-swiper"><i class="icon-uniE6D8 icon-large"></i></div>
 
		<div class="pagination"></div>
		<div class="swiper-wrapper"> 

      <?php 
        if($gallery) {
          foreach ($gallery as $val) {
            if ($val->type == 0) {
      ?>
			<div class="swiper-slide">
				<div class="inner">
					<h2 class="title"><?php echo $val->title?></h2>
					<img src="<?php echo base_url() . $val->image_path  ?>" alt="" style="width: 771px; height: 514px;">
				</div>
      </div>
      <?php
            }
          }
        }
      ?>
			
		</div>
	</div>
     <section class="page-title portfolio-single v2 bg-v2">
					 <!-- Place somewhere in the <body> of your page -->
       <div class="container">

         <div class="row">
           <div class="col-lg-8 col-md-8 col-sm-12 col-xm-12 center-col">
             <article class="portfolio-content-container">
               <h3 class="title">How were you introduced to Peepwool</h3>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Curabitur blandit tempus porttitor.</p>
               <p>Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Curabitur blandit tempus porttitor. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Nullam quis risus eget urna mollis ornare vel eu leo. Cras mattis consectetur purus sit amet fermentum.</p>
               <p>Vestibulum id ligula porta felis euismod semper. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Etiam porta sem malesuada magna mollis euismod. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Nulla vitae elit libero, a pharetra augue. <br>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
             </article>
             <div class="portfolio-tags">
               <ul class="list-unstyled list-inline tags-list">
                 <li><a href="#">Photography</a></li>
                 <li><a href="#">Motion</a></li>
                 <li><a href="#">Design</a></li>
               </ul>
             </div>

             <div class="portfolio-socials">
               <ul class="list-inline meta-list center">
                <li><a href="#"><span class="icn-container"><i class="  icon-heart"></i></span> 34</a></li>
                <li><a href="#"><span class="icn-container"><i class=" icon-facebook"></i></span> 14</a></li>
                <li><a href="#"><span class="icn-container"><i class=" icon-twitter"></i></span> 14</a></li>
                <li><a href="#"><span class="icn-container"><i class=" icon-pinterest"></i></span> 14</a></li>
               </ul>
             </div>

           </div>
         </div>

       </div>
     </section> <!-- end page-title -->


    <!-- 
       /*
       |**************************************************************************
       |  Comments
       |  
       |**************************************************************************
       |
       | 
       |
       */
    -->

     <section class="comments">
       <div class="container">
         <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <div class="comment-title">
               <h1 class="title">Comment</h1>
             </div>
           </div>
         </div>
         <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <ul class="media-list">
               <li class="media">
                 <a class="pull-left" href="#">
                   <img src="<?php echo base_url('assets/img/expertise/avatar.png') ?>" alt="">
                 </a>
                 <div class="media-body">
                   <h4 class="media-heading">Anders <span class="frase">says:</span> </h4>
                   <div class="media-content">
                     <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur.</p>

                     <ul class="list-unstyled list-inline comment-information">
                       <li class="pull-left">Sep 14th, 2013 at 12:21 pm</li>
                       <li class="pull-right"><a href="#" class="reply"><i class="  icon-reply"></i></a></li>
                     </ul>
                     
                   </div>
                   <!-- Nested media object -->
                   <div class="media">
                     <a class="pull-left" href="#">
                       <img src="<?php echo base_url('assets/img/expertise/avatar2.png') ?>" alt="">
                     </a>
                     <div class="media-body">
                       <h4 class="media-heading">Sara <span class="frase">says:</span> </h4>
                       <div class="media-content">
                         
                         <p>Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. Cras purus odio, vestibulum in vulputate at, tempus viverra turpis. Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur.</p>

                         <ul class="list-unstyled list-inline comment-information">
                           <li class="pull-left">Sep 14th, 2013 at 12:21 pm</li>
                           <li class="pull-right"><a href="#" class="reply"><i class="  icon-reply"></i></a></li>
                         </ul>
                       </div>
                       
                     </div>
                   </div>
                   
                 </div>
               </li>
               
             </ul>
           </div>
           
         </div>
       </div>
     </section>

     <!-- 
        /*
        |**************************************************************************
        |  Comments Form
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->
    
    <section class="comments-form">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="comments-title center">
              <h2 class="title">Leave a reply</h2>
            </div>
          </div>
        </div>
        <div class="form-container">
          <form role="form">
            <div class="row">
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label for="name">Name</label>
                  <input type="text" id="name" class="form-control" required>
                </div>
              </div>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label for="exampleInputEmail1">Email</label>
                  <input type="email" class="form-control" id="exampleInputEmail1" required>
                </div>
              </div>
              <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="form-group">
                  <label for="webpage">Webpage</label>
                  <input type="text" id="webpge" class="form-control">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <label for="msg">Message</label>
                <textarea name="message" id="msg" cols="30" rows="10" class="form-control" required></textarea>
                <button type="submit" class="btn btn-light-gray btn-xxlarge pull-right">Send message</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
    
<?php $this->load->view('frontend/_partial/bottom'); ?>
<?php $this->load->view('frontend/_partial/footer'); ?>