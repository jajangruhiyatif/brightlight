
<?php $this->load->view('frontend/_partial/header'); ?>

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/vendor/perfect-scrollbar/perfect-scrollbar.css') ?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/util.css') ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/main-2.css') ?>">
    
<?php $this->load->view('frontend/_partial/navbar'); ?>
<section class="page-title color-scheme3">
      

	<!-- <div class="limiter"> -->
		<div class="container-table100">
			<div class="wrap-table100">
				<div class="table100 ver1 m-b-110">
					<div class="table100-head">
						<table>
							<thead>
								<tr class="row100 head">
									<th class="cell100 column1">Daerah</th>
									<th class="cell100 column2">Tipe</th>
									<th class="cell100 column3">Merek</th>
									<th class="cell100 column4">Durasi</th>
									<th class="cell100 column5">Harga</th>
								</tr>
							</thead>
						</table>
					</div>

					<div class="table100-body js-pscroll">
						<table>
							<tbody>

								<?php 
									if($rent) {
										foreach ($rent as $val) {
								?>
								
								<tr class="row100 body">
									<td class="cell100 column1"><?php echo $val->region ?></td>
									<td class="cell100 column2"><?php echo $val->vehicle_type ?></td>
									<td class="cell100 column3"><?php echo $val->vehicle_brand ?></td>
									<td class="cell100 column4"><?php echo $val->duration ?></td>
									<td class="cell100 column5"><?php echo $val->price ?></td>
								</tr>

								<?php 
										}
									}
								?>

							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	<!-- </div> -->

</section>


<?php $this->load->view('frontend/_partial/bottom'); ?>
	
	<script src="<?php echo base_url('assets/vendor/bootstrap/js/popper.js') ?>"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/vendor/select2/select2.min.js') ?>"></script>
	<!--===============================================================================================-->
	<script src="<?php echo base_url('assets/vendor/perfect-scrollbar/perfect-scrollbar.min.js') ?>"></script>

<?php $this->load->view('frontend/_partial/footer'); ?>