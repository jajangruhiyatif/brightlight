<?php $this->load->helper("url"); ?>
<?php $this->load->view('frontend/_partial/header'); ?>
<?php $this->load->view('frontend/_partial/navbar'); ?>

     <section class="page-title bg-version">
       <div class="container">
         <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<?php 
					foreach ($about as $val) {
						if($val->type == 3) {
				?>
			  	<h1 class="title center">
					  <?php 
					  		echo str_replace(";", "<br>", $val->content_2);
							// echo $val->content_2;
						?>
				</h1>
				
			  <?php
						}
					}
			  ?>
            </div>
         </div>
       </div>
     </section> <!-- end page-title -->


  <!-- 
     /*
     |**************************************************************************
     |  About us Section
     |  
     |**************************************************************************
     |
     | 
     |
     */
  -->

  <section class="about-container-main-tagline">
    <div class="container">
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h3 class="title center">Your Pixel Pushers</h3>
        </div>
      </div>
    </div>
  </section>


  <!-- 
     /*
     |**************************************************************************
     |  Team Carousel
     |  
     |**************************************************************************
     |
     | 
     |
     */
  -->

		<div class="swiper-container" id="about-carousel">
				<div class="swiper-about-next"><a href="#"><i class="icon-uniE6D8 icon-large"></i></a></div>
				<div class="swiper-about-prev"><a href="#"><i class="icon-arrow-left7 icon-large"></i></a></div>
				
		        <div class="swiper-wrapper">
						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/1.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/2.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/3.jpg') ?>" alt="img" class="img-responsive">
							  </figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/4.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/5.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/6.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/7.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/8.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->

						<div class="swiper-slide">
							  <div class="item">
								<div class="team-carousel">
								  <div class="figure-container">
									<figure class="team-img">
									  <img src="<?php echo base_url('assets/img/team/9.jpg') ?>" alt="img" class="img-responsive">
									</figure>
									<div class="team-description">
									  <ul class="list-unstyled team-info">
										<li class="name">Another <br> Matador</li>
										<li class="specs">Digital</li>
									  </ul>
									</div>
								  </div>          
								  <div class="team-socials center">
									<ul class="list-unstyled list-inline list-socials">
									  <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
									  <li><a href="#"><span class="icn-container"><i class="icon-dribbble icon-large"></i></span> </a></li>
									</ul>
								  </div>
								</div>
							  </div>
					  </div> <!-- end swiper-slide -->
					  
				</div> <!-- end swiper-wrapper -->
		</div> <!-- end swiper-container -->


  <!-- 
      /*
      |**************************************************************************
      |  About Description
      |  
      |**************************************************************************
      |
      | 
      |
      */
   -->
  
  <section class="about-description">
    <div class="container">
      <div class="row">
		
		<?php 
			if($about) {
				foreach ($about as $val) {
					if($val->type == 0 || $val->type == 1 || $val->type == 2) {
		?>

	  	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="about-description-content">
            <h3 class="title"><?php echo $val->content_1 ?></h3>
            <p><?php echo $val->content_2 ?></p>
          </div>
        </div>
		<?php
					}
				}
			}
		?>
        <!-- <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="about-description-content">
            <h3 class="title">What We Offer</h3>
            <p>Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur. </p>
          </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
          <div class="about-description-content">
            <h3 class="title">The process</h3>
            <p>Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur. </p>
          </div>
		</div> -->
		
      </div>
      <br><br><br>
      <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 center-col center">
          <a href="contact" class="btn btn-light-gray btn-xxlarge">Contact Us</a>
        </div>
      </div>
    </div>
  </section>


  <!-- 
      /*
      |**************************************************************************
      | Expertise bars
      |  
      |**************************************************************************
      |
      | 
      |
      */
   -->

  <section class="expertise-bars">
    <div class="container">
      <div class="row">
        <div class="expertise-title center  ">
          <h3 class="title">Our Skills</h3>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="progress-bar-container">
            <h5 class="title">Branding</h5>
            <div class="progress">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100" style="width: 90%;">
              </div>
            </div>
            <ul class="list-unstyled list-inline list-skill-levels center">
              <li>9</li>
              <li>4</li>
              <li>%</li>
            </ul>

          </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="progress-bar-container">
            <h5 class="title">Graphic Design</h5>
            <div class="progress">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="82" aria-valuemin="0" aria-valuemax="100" style="width: 82%;">
              </div>
            </div>
            <ul class="list-unstyled list-inline list-skill-levels center">
              <li>8</li>
              <li>2</li>
              <li>%</li>
            </ul>

          </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="progress-bar-container">
            <h5 class="title">iOS Development</h5>
            <div class="progress">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="42" aria-valuemin="0" aria-valuemax="100" style="width: 42%;">
              </div>
            </div>
            <ul class="list-unstyled list-inline list-skill-levels center">
              <li>4</li>
              <li>2</li>
              <li>%</li>
            </ul>

          </div>
        </div>

        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="progress-bar-container">
            <h5 class="title">Development</h5>
            <div class="progress">
              <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
              </div>
            </div>
            <ul class="list-unstyled list-inline list-skill-levels center">
              <li>7</li>
              <li>5</li>
              <li>%</li>
            </ul>

          </div>
        </div>

      </div>
    </div>
  </section>

<?php $this->load->view('frontend/_partial/testimony'); ?>
<?php $this->load->view('frontend/_partial/bottom'); ?>
<?php $this->load->view('frontend/_partial/footer'); ?>