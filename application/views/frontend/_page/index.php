<?php $this->load->helper("url"); ?>
<?php $this->load->view('frontend/_partial/header'); ?>
<?php $this->load->view('frontend/_partial/navbar'); ?>

      <section class="layerslider-container ">

        <div id="layerslider-container-fw">
          
          <div id="layerslider" style="width: 100%; height: 870px; margin: 0px auto; ">

              <?php 
                foreach($data as $val) {
                  if($val->type == 0) {
              ?>

            <div class="ls-layer" style="transition2d: all; ">

              <img src="<?php echo base_url('assets/img/slides/slide1.png') ?>" class="ls-bg" alt="Slide background">
              <img class="ls-s2" src="<?php echo base_url('assets/img/slides/iphone.png') ?>" style="position: absolute; top: 420px; left: 
              260px; slidedirection : bottom; slideoutdirection : bottom;  durationin : 2000; durationout : 2000; easingin : easeInQuart; easingout : easeOutQuart; delayin : 100; delayout : 0; showuntil : 6000; ">
              <h1 class="ls-s-1 text" style=" top:180px; left: 140px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> 
                <!-- Let’s build something  -->
                <?php 
                  echo $val->content_1;
                ?>
              </h1>

              <h1 class="ls-s-1 text" style=" top:250px; left: 190px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> 
                <!-- beautiful together -->
                <?php
                  echo $val->content_2;
                ?>
              </h1>
              
            </div>

            <div class="ls-layer" style="  transition2d: all; ">

              <img src="<?php echo base_url('assets/img/slides/slide2.png') ?>" class="ls-bg" alt="Slide background">

               <img class="ls-s2" src="<?php echo base_url('assets/img/slides/ipad.png') ?>" style="position: absolute; top: 420px; left: 170px;  slidedirection : bottom; slideoutdirection : bottom;  durationin : 1500; durationout : 1500; easingin : easeInQuart; easingout : easeOutQuart;  delayin : 150; delayout : 0; showuntil : 6000; ">

               <h1 class="ls-s-1 text" style=" top:180px; left: 140px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> 
               <?php
                echo $val->content_1;
               ?>
                <!-- Let’s build something  -->
              </h1>

              <h1 class="ls-s-1 text" style=" top:250px; left: 190px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> 
              <?php 
                echo $val->content_2;
              ?>
                <!-- beautiful together -->
              </h1>
              

            </div>

            <div class="ls-layer"  style=" transition2d: all;  ">

              <img src="<?php echo base_url('assets/img/slides/slide3.jpg') ?>" class="ls-bg" alt="Slide background">

              <img class="ls-s2" src="<?php echo base_url('assets/img/slides/ipad.png') ?>" style="position: absolute; top: 420px; left: 
              170px;  slidedirection : bottom; slideoutdirection : bottom;  durationin : 1500; durationout : 1500; easingin : easeInQuart; easingout : easeOutQuart;  delayin : 150; delayout : 0; showuntil : 6000; ">

               <h1 class="ls-s-1 text" style=" top:180px; left: 140px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> 
               <?php
                  echo $val->content_1;
               ?>
                <!-- Let’s build something  -->
              </h1>

              <h1 class="ls-s-1 text" style=" top:250px; left: 190px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> 
              <?php 
                echo $val->content_2;
              ?>
                <!-- beautiful together -->
              </h1>
              <?php
                  }
                }
              ?>
            </div>

          </div>
                
        </div> <!--end layerslider-->
           
          </div> <!-- end row-fluid -->
        </section> <!-- end layerslder-container -->

     <!-- 
        /*
        |**************************************************************************
        |  Tagline
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

     <section class="tag-line">
       <div class="container">
         <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <?php
              foreach ($data as $val) {
                if ($val->type == 1) {
            ?>

             <h2 class="center subtitle"><?php echo $val->content_1 ?> </h2>
             <h1 class="center title"><?php echo $val->content_2 ?></h1>

             <?php
                }
              }
            ?>

           </div> <!-- end cols -->
         </div> <!-- end row -->
         <div class="row">
         <div class="col-lg-12">
           <div class="row">
             <div class="tag-info">

               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tag-info-bx1">
                   <h4 class="subtitle center">Have a project in mind?</h4>
                   <p class="center"><a class="btn btn-light-gray btn-lg" href="contact.html">instant contact</a> </p>
                </div> <!-- end tag-info-bx1 -->
               </div> <!-- end col -->
               <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                 <div class="tag-info-bx2">
                   <h4 class="subtitle center">Say Hej!</h4>
                   <h3 class="center title">hej@brightlight.com</h3>
                  </div> <!-- end box-2 -->
               </div> <!-- end col -->
               <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                 <div class="tag-info-bx1">
                   <h4 class="subtitle center">Follow us on</h4>
                   <ul class="list-unstyled list-inline socials center">
                     <li><a href="#"><span class="icn-container"><i class="icon-facebook icon-large"></i></span> </a></li>
                     <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> </a></li>
                     <li><a href="#"><span class="icn-container"><i class="icon-instagram icon-large"></i></span> </a></li>
                     <li><a href="#"><span class="icn-container"><i class="icon-pinterest icon-large"></i></span> </a></li>
                   </ul>
                 </div>
               </div> <!-- end col -->
             </div> <!-- end tag-info -->
             
           </div> <!-- end row -->
         </div> <!-- end col -->
         </div> <!-- end row -->
       </div> <!-- end container -->
     </section> <!-- end tag-line -->

      <!-- 
        /*
        |**************************************************************************
        |  Expertise Section
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

     <section class="expertise">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="expertise-titles">

            <?php 
              foreach ($data as $val) {
                if ($val->type == 2) {
            ?>
            
              <h1 class="title center"><?php echo $val->content_1 ?></h1>
              <h4 class="sub-title center"><?php echo $val->content_2 ?></h4>
              <h4 class="sub-title center"><?php echo $val->content_3 ?></h4>

            <?php
                }  
              }
            ?>

            </div>
          </div> <!-- end cols -->
        </div> <!-- end row -->
        <div class="row">
          <div class="expertise-columns">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="expertise-content-container">
                <div class="icn-main-container">
                  <p class="center"><span class="icn-container"><i class="icon-mobile icon-large"></i></span></p>
                </div> <!-- end icn-main-container -->
                <?php 
                  foreach($data as $val) { 
                    if ($val->type == 4) {
                ?>
                <h3 class="title center"><?php echo $val->content_1 ?><br> <?php echo $val->content_2 ?></h3>
                <p class="center content"><?php echo $val->content_3 ?></p>
                <?php
                    }
                  }
                ?>

                <p class="center"><a class="btn btn-light-gray btn-lg" href="price_list">Read More</a> </p>
              </div> <!-- end expertise-content-container -->
            </div> <!-- end col=* -->

            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="expertise-content-container">
                <div class="icn-main-container">
                  <p class="center"><span class="icn-container"><i class="icon-docs icon-large"></i></span></p>
                </div> <!-- end icn-main-container -->
                <?php 
                  foreach($data as $val) {
                    if($val->type == 5) {
                ?>
                <h3 class="title center"><?php echo $val->content_1 ?><br> <?php echo $val->content_2 ?></h3>
                <p class="center content"><?php echo $val->content_3 ?></p>
                <?php
                    }
                  }
                  ?>
                <p class="center"><a class="btn btn-light-gray btn-lg" href="tour_package">Read More</a> </p>
              </div> <!-- end expertise-content-container -->
            </div> <!-- end col=* -->
          </div> <!-- end expertise-columns -->
        </div> <!-- end row -->
      </div> <!-- end container -->
     </section> <!-- end expertise -->

     <!-- 
        /*
        |**************************************************************************
        |  Latest Work
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

    <section class="latest-work">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="latest-work-titles">
              <?php 
                foreach($data as $val) {
                  if($val->type == 3) {
                ?>
              <h1 class="title center"><?php echo $val->content_1 ?></h1>
              <h3 class="sub-title center"><?php echo $val->content_2 ?></h3>
              <h3 class="sub-title center"><?php echo $val->content_3 ?></h3>
              <?php
                  }
                }
              ?>
            </div>
          </div>
        </div>
      </div>
    </section> <!-- end section-latest -->

    <!-- 
        /*
        |**************************************************************************
        |  Latest Work Columns
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

    <section class="latest-work-columns">
      <div class="container">
        <div class="row">
          <div class="portfolio-isotope">
            <div class="portfolio-items">
              <div class="portfolio-items-container">
                <figure class="item-image">
                  <img src="<?php echo base_url('assets/img/work/1.jpg') ?>" alt="img" class="img-responsive">
                  <div class="overlay-container">
                    <div class="content-title">
                      <p class="center"><span class="icn-container"><i class="icon-pictures icon-large"></i></span></p>
                    </div>
                    <div class="content-container">
                      <h3 class="title center">Ullamcorper Porta <br> Nibh Bibendum</h3>
                    </div>
                    <div class="content-footer">
                      <p class="center"><a href="portfolio-single.html" class="btn btn-danger btn-lg">Responsive Design</a></p>
                    </div>
                  </div>
                </figure>
              </div>

              <div class="portfolio-items-container">
                <figure class="item-image">
                  <img src="<?php echo base_url('assets/img/work/2.jpg') ?>" alt="img" class="img-responsive">
                  <div class="overlay-container">
                    <div class="content-title">
                      <p class="center"><span class="icn-container"><i class="icon-pictures icon-large"></i></span></p>
                    </div>
                    <div class="content-container">
                      <h3 class="title center">Ullamcorper Porta <br> Nibh Bibendum</h3>
                    </div>
                    <div class="content-footer">
                      <p class="center"><a href="portfolio-single.html" class="btn btn-danger btn-lg">Responsive Design</a></p>
                    </div>
                  </div>
                </figure>
              </div>

              <div class="portfolio-items-container">
                <figure class="item-image">
                  <img src="<?php echo base_url('assets/img/work/3.jpg') ?>" alt="img" class="img-responsive">
                  <div class="overlay-container">
                    <div class="content-title">
                      <p class="center"><span class="icn-container"><i class="icon-pictures icon-large"></i></span></p>
                    </div>
                    <div class="content-container">
                      <h3 class="title center">Ullamcorper Porta <br> Nibh Bibendum</h3>
                    </div>
                    <div class="content-footer">
                      <p class="center"><a href="portfolio-single.html" class="btn btn-danger btn-lg">Responsive Design</a></p>
                    </div>
                  </div>
                </figure>
              </div>

              <div class="portfolio-items-container">
                <figure class="item-image">
                  <img src="<?php echo base_url('assets/img/work/4.jpg') ?>" alt="img" class="img-responsive">
                  <div class="overlay-container">
                    <div class="content-title">
                      <p class="center"><span class="icn-container"><i class="icon-pictures icon-large"></i></span></p>
                    </div>
                    <div class="content-container">
                      <h3 class="title center">Ullamcorper Porta <br> Nibh Bibendum</h3>
                    </div>
                    <div class="content-footer">
                      <p class="center"><a href="portfolio-single.html" class="btn btn-danger btn-lg">Responsive Design</a></p>
                    </div>
                  </div>
                </figure>
              </div>

              <div class="portfolio-items-container">
                <figure class="item-image">
                  <img src="<?php echo base_url('assets/img/work/5.jpg') ?>" alt="img" class="img-responsive">
                  <div class="overlay-container">
                    <div class="content-title">
                      <p class="center"><span class="icn-container"><i class="icon-pictures icon-large"></i></span></p>
                    </div>
                    <div class="content-container">
                      <h3 class="title center">Ullamcorper Porta <br> Nibh Bibendum</h3>
                    </div>
                    <div class="content-footer">
                      <p class="center"><a href="portfolio-single.html" class="btn btn-danger btn-lg">Responsive Design</a></p>
                    </div>
                  </div>
                </figure>
              </div>

              <div class="portfolio-items-container">
                <figure class="item-image">
                  <img src="<?php echo base_url('assets/img/work/6.jpg') ?>" alt="img" class="img-responsive">
                  <div class="overlay-container">
                    <div class="content-title">
                      <p class="center"><span class="icn-container"><i class="icon-pictures icon-large"></i></span></p>
                    </div>
                    <div class="content-container">
                      <h3 class="title center">Ullamcorper Porta <br> Nibh Bibendum</h3>
                    </div>
                    <div class="content-footer">
                      <p class="center"><a href="portfolio-single.html" class="btn btn-danger btn-lg">Responsive Design</a></p>
                    </div>
                  </div>
                </figure>
              </div>

              <div class="portfolio-items-container">
                <figure class="item-image">
                  <img src="<?php echo base_url('assets/img/work/7.jpg') ?>" alt="img" class="img-responsive">
                  <div class="overlay-container">
                    <div class="content-title">
                      <p class="center"><span class="icn-container"><i class="icon-pictures icon-large"></i></span></p>
                    </div>
                    <div class="content-container">
                      <h3 class="title center">Ullamcorper Porta <br> Nibh Bibendum</h3>
                    </div>
                    <div class="content-footer">
                      <p class="center"><a href="portfolio-single.html" class="btn btn-danger btn-lg">Responsive Design</a></p>
                    </div>
                  </div>
                </figure>
              </div>

            </div> <!-- end portfolio-items -->
          </div> <!-- end portfolio-isotope -->
        </div> <!-- end row -->
        <br>
        <br>
        <br>
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p class="center"><a class="btn btn-light-gray btn-xxlarge">Read More</a> </p>
          </div>
        </div>
        <br>
      </div> <!-- end container -->
    </section> <!-- end latest -->

    <?php $this->load->view('frontend/_partial/testimony'); ?>
      <!-- 
        /*
        |**************************************************************************
        |  Clients Slider
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

     <section class="clients-section">
       <div class="container">
         <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <!-- Place somewhere in the <body> of your page -->
             <div class="flexslider" id="Clients-Carousel">
               <ul class="slides">
                 <li>
                  <a href="#"><img src="<?php echo base_url('assets/img/clients/1.png') ?>" alt="img"></a>
                 </li>
                 <li>
                  <a href="#"><img src="<?php echo base_url('assets/img/clients/2.png') ?>" alt="img"></a>
                 </li>
                 <li>
                  <a href="#"><img src="<?php echo base_url('assets/img/clients/3.png') ?>" alt="img"></a>
                 </li>
                 <li>
                   <a href="#"><img src="<?php echo base_url('assets/img/clients/4.png') ?>" alt="img"></a>
                 </li>
                 <li>
                   <a href="#"><img src="<?php echo base_url('assets/img/clients/5.png') ?>" alt="img"></a>
                 </li>
                 <li>
                  <a href="#"><img src="<?php echo base_url('assets/img/clients/2.png') ?>" alt="img"></a>
                 </li>
                 <li>
                  <a href="#"><img src="<?php echo base_url('assets/img/clients/3.png') ?>" alt="img"></a>
                 </li>
                 <li>
                   <a href="#"><img src="<?php echo base_url('assets/img/clients/4.png') ?>" alt="img"></a>
                 </li>
                 <!-- items mirrored twice, total of 12 -->
               </ul>
             </div>
           </div>
         </div>
       </div>
     </section>

<?php $this->load->view('frontend/_partial/bottom'); ?>
<?php $this->load->view('frontend/_partial/footer'); ?>
