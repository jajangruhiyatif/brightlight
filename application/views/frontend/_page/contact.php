<?php $this->load->helper("url"); ?>
<?php $this->load->view('frontend/_partial/header'); ?>
<?php $this->load->view('frontend/_partial/navbar'); ?>


      <section class="layerslider-container ">
            
        <div id="layerslider-container-fw">
          
          <div id="layerslider2" style="width: 100%; height: 570px; margin: 0px auto; ">

            <div class="ls-layer" style="transition2d: all; ">

              <img src="img/slides/slide4.jpg" class="ls-bg" alt="Slide background">

              <h1 class="ls-s-1 text" style=" top:220px; left: 220px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> Heralding from </h1>

              <h1 class="ls-s-1 text" style=" top:290px; left: 170px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> Salt Lake City, Utah</h1>


            </div>

            <div class="ls-layer" style="  transition2d: all; ">

              <img src="img/slides/slide5.jpg" class="ls-bg" alt="Slide background">

               <h1 class="ls-s-1 text" style=" top:220px; left: 120px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> Let’s build something </h1>

              <h1 class="ls-s-1 text" style=" top:290px; left: 170px; slidedirection : fade; slideoutdirection : fade; durationin : 1500; durationout : 1500; easingin : easeOutQuint; scalein : .5; scaleout : .5; showuntil : 4000;"> beautiful together</h1>


            </div>

          </div>
                
        </div> <!--end layerslider-->
           
          </div> <!-- end row-fluid -->
        </section> <!-- end layerslder-container -->

     <!-- 
        /*
        |**************************************************************************
        |  Tagline
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

     <section class="tag-line">
       <div class="container">
        <div class="row">
         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
           <ul class="list-unstyled list-inline contact-list socials">
             <li><a href="#"><span class="icn-container"><i class="icon-map-marker icon-large"></i></span> WLS Address  252 Edison St. SLC, UT 84111</a></li>
           </ul>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
           <ul class="list-unstyled list-inline contact-list socials">
              <li><a href="#"><span class="icn-container"><i class="icon-envelope icon-large"></i></span>hey@brightlight.com</a></li>
              <li><a href="#"><span class="icn-container"><i class="icon-twitter icon-large"></i></span> @brightlight</a></li>
           </ul>
         </div>
         <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
           <ul class="list-unstyled list-inline contact-list socials">
             <li><a href="#"><span class="icn-container"><i class="icon-trophy icon-large"></i></span> Award Winning Digital Creative Agency</a></li>
           </ul>
         </div>
        </div> 
       </div> <!-- end container -->
     </section> <!-- end tag-line -->

     <!-- 
        /*
        |**************************************************************************
        |  Google Map
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

     <section class="contact-map">
       <iframe width="100%" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps/ms?msa=0&amp;msid=217101749085092333090.0004cc4cc0c93a10d81cd&amp;ie=UTF8&amp;ll=36.165319,-86.784611&amp;spn=0,0&amp;t=m&amp;output=embed"></iframe>
     </section>

  <!-- 
        /*
        |**************************************************************************
        |  Contact Form
        |  
        |**************************************************************************
        |
        | 
        |
        */
     --> 

  <section class="contact-form">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
          <div class="contact-form-container">
            <form role="form">
              <div class="form-group">
                <label for="name">Your Name</label>
                <input type="text" class="form-control" id="name">
              </div>
              <div class="form-group">
                <label for="emal">Your Email</label>
                <input type="email" class="form-control" id="emal" >
              </div>
              <div class="form-group">
                <label for="subjet">Subject</label>
                <input type="text" id="subjet" class="form-control">
              </div>
              <div class="form-group">
                <label for="message">Your Message</label>
                <textarea name="message" id="message" cols="30" rows="10"></textarea>
              </div>
              <button type="submit" class="btn btn-light-gray btn-xxlarge pull-right">Send Message</button>
            </form>
          </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
          <div class="contact-information">
            <h3 class="title">Don’t hesitate to reach out!</h3>
            <p>Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa. </p>
            <p>Justo sit amet risus. Aenean lacinia bibendum nulla sed consecteturDonec id elit non mi porta gravida at eget metus. Maecenas faucibus mollis interdum. Donec sed odio dui. Donec sed odio dui. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</p>
          </div>
        </div>
      </div>
    </div>
  </section>
  
<?php $this->load->view('frontend/_partial/bottom'); ?>
<?php $this->load->view('frontend/_partial/footer'); ?>