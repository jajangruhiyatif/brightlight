<?php $this->load->helper("url"); ?>
<?php $this->load->view('frontend/_partial/header'); ?>
<?php $this->load->view('frontend/_partial/navbar'); ?>

     <section class="page-title color-scheme3">
       <div class="container">
         <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
              <div class="content-container">
                <h1 class="title">Sed posuere consectetur est at lobortis. </h1>
                <ul class="list-unstyled author list-inline">
                  <li><img src="<?php echo base_url('assets/img/expertise/author.png') ?>" alt="img"></li>
                  <li>By Joe in <a href="#">Design</a> Sep 14th 2013</li>
                </ul>
                <ul class="list-unstyled list-inline">
                  <li><a class="btn btn-light-gray btn-xxlarge">Read More</a></li>
                </ul>
              </div>
              
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
              <article class="post-content">
                <div class="post-slider">
                  <div class="flexslider theme3" id="article-slide">
                    <ul class="slides">
                      <?php 
                        if($data) {
                          foreach ($data as $val) {
                      ?>
                      <li>
                        <a href="#"><img src="<?php echo base_url('') . $val->image_path ?>" alt="img" width="500" height="500"></a>
                      </li>

                      <?php 
                          }
                        }
                      ?>

                    </ul>

                  </div>
                </div>
                
              </article>
            </div>
         </div>
       </div>
     </section> <!-- end page-title -->


    <!-- 
       /*
       |**************************************************************************
       |  Related Posts
       |  
       |**************************************************************************
       |
       | 
       |
       */
    -->

    <section class="related-posts">
      <div class="container">
        <div class="row">
        <?php 
          if($data) {
            foreach ($data as $val) {
        ?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="related-post-container">
              <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <div class="category-container center">
                    <ul class="list-unstyled">
                      <li><a href="#" class="btn btn-danger">Rp. <?php echo $val->price ?> </a></li>
                    </ul>
                  </div>
                </div>

                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                  <div class="related-pic-container center">
                    <figure class="related-pic">
                      <img src="<?php echo base_url() . $val->image_path ?>" alt="img" width="120" height="120">
                    </figure>
                  </div>
                </div>

                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                  <div class="related-content">
                    <h3 class="title"><?php echo $val->title ?></h3>
                    <p><?php echo $val->description ?></p>
                    <!-- <p>Here is the new series of German photographer Matthias Heiderich we have talked about several times. Called ‘Reflexiones’, these stunning images of architecture...</p> -->
                  </div>
                </div>

                <div class="col-lg-1 col-md-1 col-sm-12 col-xs-12">
                  <div class="related-action center">
                    <ul class="list-unstyled">
                      <li><a href="#"><i class="entypo chevron-thin-right"></i></a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <?php
              }
            }
          ?>

      </div>
    </section>

<?php $this->load->view('frontend/_partial/bottom'); ?>
<?php $this->load->view('frontend/_partial/footer'); ?>
