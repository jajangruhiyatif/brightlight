
     <!-- 
        /*
        |**************************************************************************
        |  Footer
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

     <footer>
       <div class="container">
         <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="widget">
              <?php 
                if ($about) {
                  foreach ($about as $val) {
                    if($val->type == 0) {
              ?>
              <h3 class="title"><?php echo $val->content_1 ?></h3>
              <p class="content"><?php echo $val->content_2?></p>
              <?php 
                    }
                  }
                }
              ?>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="widget">
              <h3 class="title">Follow us on</h3>
              <ul class="list-unstyled list-inline socials">
               <li><a href="#"><span class="icn-container center"><i class="icon-facebook icon-large"></i></span> </a></li>
               <li><a href="#"><span class="icn-container center"><i class="icon-twitter icon-large"></i></span> </a></li>
               <li><a href="#"><span class="icn-container center"><i class="icon-instagram icon-large"></i></span> </a></li>
               <li><a href="#"><span class="icn-container center"><i class="icon-pinterest icon-large"></i></span> </a></li>
             </ul>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="widget">
              <h3 class="title">Recent Articles</h3>
              <ul class="recents list-unstyled">
                <?php if($activity) {
                  foreach ($activity as $val) {

                  ?>
                <li>
                  <a href="tour_package" style="overflow: hidden; 
                                    text-overflow: ellipsis; 
                                    display: -webkit-box; 
                                    -webkit-line-clamp: 2;  
                                    -webkit-box-orient: vertical;">
                                    
                    <?php echo $val->description ?>
                  </a>
                </li>
                <?php 
                  }
                }
                ?>
              </ul>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <div class="widget">
              <h3 class="title">Latest on Flickr</h3>
              <ul class="list-unstyle list-inline flickr-imgs">
            </div>
          </div>
           
         </div>
         <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="sub-footer-container">
              <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                  <a href="#" class="footer-logo"><img src="<?php echo base_url('assets/img/logo.png') ?>" alt="logo"></a>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                  <nav>
                    <ul class="list-unstyled list-inline pull-right footer-nav">
                      <li class="active"><a href="#">Home</a></li>
                      <li><a href="#">Price List</a></li>
                      <li><a href="#">Gallery</a></li>
                      <li><a href="#">Paket Wisata</a></li>
                      <li><a href="#">About us</a></li>
                      <li><a href="#">Contact</a></li>
                    </ul>
                  </nav>
                </div>
              </div>
            </div>
          </div>
         </div>
       </div>
     </footer>