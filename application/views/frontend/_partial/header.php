<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="moosethemes">
    <meta name="description" content="">
    <link rel="shortcut icon" href="ico/favicon.png">

    <title>Bright Light</title>
    
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('assets/css/component.css'); ?>">

    <!-- Font Awesome/icon-CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/fonts.css'); ?>">

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,300,600,700' rel='stylesheet' type='text/css'>

    <!-- LayerSlider styles -->
    <link rel="stylesheet" href="<?php echo base_url('assets/layerslider/css/layerslider.css') ?>" type="text/css">

    <!-- carousel styles about_us-->
	  <link rel="stylesheet" href="<?php echo base_url('assets/css/idangerous.swiper.css') ?>">
    
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/css/styles.css') ?>" rel="stylesheet">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<script src="<?php echo base_url('assets/js/plugins/modernizr.custom.js') ?>"></script>
  