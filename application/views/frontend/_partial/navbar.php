</head>

  <body>

    <!-- 
        /*
        |**************************************************************************
        |  Navigation
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

      <!-- Static navbar -->
    <div class="navbar-wrapper">
      <div class="container">
        <div class="navbar navbar-default">
          <div class="navbar-header">
            <a class="navbar-brand" href="index"><img src="<?php echo base_url('assets/img/logo.png') ?>" alt="logo"></a>
          </div>
		     <div> 
				  <div class="responsive-menu">	
					<div class="main clearfix">
						<div class="column">
							<div id="dl-menu" class="dl-menuwrapper">
								<button class="dl-trigger">Open Menu</button>
								<ul class="dl-menu">
									<li> <a href="index">Home</a> </li>
									<li><a href="portfolio-4c.html">Price list</a></li>
									<li><a href="portfolio-single-gallery.html">Gallery</a></li>
									<li><a href="tour_package">Tour package</a></li>
									<li><a href="about">About us</a></li>
									<li><a href="contact">Contact</a></li>
								</ul>
							</div><!-- /dl-menuwrapper -->
						</div> <!-- end main -->
					</div> <!-- demo -->
				</div><!-- /container -->
            <ul class="nav navbar-nav navbar-right">
              <li class="active"><a href="index">Home</a></li>
              <li><a href="price_list">Price list</a></li>
			        <li><a href="gallery">Gallery</a></li>
              <li><a href="tour_package">Paket wisata</a></li>
              <li><a href="about">About us</a></li>
              <li><a href="contact">Contact</a></li>
              <li class="sb-container">
				<div id="sb-search" class="sb-search">
					<form>
						<input class="sb-search-input" placeholder="Enter your search term..." type="search" value="" name="search" id="search">
						<input class="sb-search-submit" type="submit" value="">
						<span class="sb-icon-search icon-flip-horizontal"></span>
					</form>
				</div>
			  </li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div> <!-- end container -->
    </div> <!-- end nav-wrapper -->
