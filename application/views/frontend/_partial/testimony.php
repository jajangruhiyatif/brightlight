<!-- 
        /*
        |**************************************************************************
        |  Testimonials Slider
        |  
        |**************************************************************************
        |
        | 
        |
        */
     -->

     <section class="testimonials">
       <div class="container">
         <div class="row">
           <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
             <!-- Place somewhere in the <body> of your page -->
             <div class="flexslider" id="Testimonial-Carousel">
               <ul class="slides">
                 <?php
                  if($testimony) {
                    foreach ($testimony as $val ) {
                 ?>
                 <li>
                   <div class="captions center">
                      <p class="quotes"><i class="icon-quote-left icon-2x"></i></p>
                       <h3 class="title"><?php echo $val->content; ?></h3> <h3 class="title"><?php echo $val->sub_content?></h3>
                       <p class="subtitle"><?php echo $val->created_by ?></p>
                   </div> <!-- end captions -->
                 </li>
                 <!-- items mirrored twice, total of 12 -->
                 <?php
                    }
                  }
                 ?>
               </ul>
             </div>
           </div>
         </div>
       </div>
     </section>
