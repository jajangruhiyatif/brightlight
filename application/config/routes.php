<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Homepage';
$route['login'] = 'Homepage/login';

// landingpage
$route['index']         = 'Homepage';
$route['gallery']       = 'Homepage/gallery';
$route['about']         = 'Homepage/about_us';
$route['tour_package']  = 'Homepage/tour_package';
$route['price_list']    = 'Homepage/Price_list';
$route['contact']       = 'Homepage/contact';
$route['recent_activity']  = 'Homepage/recent_activity';

//--> admin page <--//
$route['home'] = 'admin/Home';
$route['home/get_data']    = 'admin/Home/get_data';
$route['home/add_data']    = 'admin/Home/add_data';
$route['home/edit_data']   = 'admin/Home/edit_data';
$route['home/delete_data'] = 'admin/Home/delete_data';

$route['testimony/index']       = 'admin/Testimony';
$route['testimony/get_data']    = 'admin/Testimony/get_data';
$route['testimony/add_data']    = 'admin/Testimony/add_data';
$route['testimony/edit_data']   = 'admin/Testimony/edit_data';
$route['testimony/delete_data'] = 'admin/Testimony/delete_data';

$route['gallery/index']       = 'admin/Gallery/index';
$route['gallery/get_data']    = 'admin/Gallery/get_data';
$route['gallery/add_data']    = 'admin/Gallery/add_data';
$route['gallery/edit_data']   = 'admin/Gallery/edit_data';
$route['gallery/delete_data'] = 'admin/Gallery/delete_data';

$route['about/index']       = 'admin/About/index';
$route['about/get_data']    = 'admin/About/get_data';
$route['about/add_data']    = 'admin/About/add_data';
$route['about/edit_data']   = 'admin/About/edit_data';
$route['about/delete_data'] = 'admin/About/delete_data';

$route['tour_package/index']        = 'admin/Tour_package/index';
$route['tour_package/get_data']     = 'admin/Tour_package/get_data';
$route['tour_package/add_data']     = 'admin/Tour_package/add_data';
$route['tour_package/edit_data']    = 'admin/Tour_package/edit_data';
$route['tour_package/delete_data']  = 'admin/Tour_package/delete_data';

$route['rent_vehicle/index']        = 'admin/Rent_vehicle/index';
$route['rent_vehicle/get_data']     = 'admin/Rent_vehicle/get_data';
$route['rent_vehicle/add_data']     = 'admin/Rent_vehicle/add_data';
$route['rent_vehicle/edit_data']    = 'admin/Rent_vehicle/edit_data';
$route['rent_vehicle/delete_data']  = 'admin/Rent_vehicle/delete_data';

$route['404_override']          = '';
$route['translate_uri_dashes']  = FALSE;
