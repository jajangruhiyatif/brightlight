-- Adminer 4.7.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `master_about`;
CREATE TABLE `master_about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_1` varchar(100) NOT NULL,
  `content_2` text NOT NULL,
  `type` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

TRUNCATE `master_about`;
INSERT INTO `master_about` (`id`, `content_1`, `content_2`, `type`) VALUES
(2,	'What We Offer',	'Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur.',	1),
(3,	'The process',	'Cras mattis consectetur purus sit amet fermentum. Nullam id dolor id nibh ultricies vehicula ut id elit. Maecenas sed diam eget risus varius blandit sit amet non magna.Donec ullamcorper nulla non metus auctor fringilla. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur.',	2),
(5,	'About us',	'Nulla porttitor accumsan tincidunt. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Vivamus suscipit tortor eget felis porttitor volutpat. Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Nulla quis lorem ut libero malesuada feugiat. Proin eget tortor risus. Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo eget malesuada. Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. Curabitur aliquet quam id dui posuere blandit.',	0),
(15,	'',	'We are available for new ; projects, so hit us',	3);

DROP TABLE IF EXISTS `master_content`;
CREATE TABLE `master_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content` varchar(150) NOT NULL DEFAULT '-',
  `sub_content` text NOT NULL,
  `type` int(11) NOT NULL,
  `page` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

TRUNCATE `master_content`;
INSERT INTO `master_content` (`id`, `content`, `sub_content`, `type`, `page`) VALUES
(1,	'Sewa Kendaraan',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta sem malesuada magna mollis euismod. Praesent commodo cursus magna, vel scelerisque nisl Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.',	4,	0),
(2,	'Paket Wisata',	'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue. Maecenas sed diam eget risus.\r\n',	5,	0),
(3,	'-',	'Our latest work ; See our few selected work , our clients appreciate ; a lot Nulla vitae elit libero, a pharetra augue. Morbi leo',	3,	0),
(4,	'-',	'This is what we do ; A creative studio where art enthusiast and tech freaks ; connects over innovative Ideas.',	2,	0),
(5,	'-',	'We help startups & companies to create great ; Web Interface, Brand & Web/iOS app design',	1,	0),
(6,	'-',	'Let’s build something ; beautiful together',	0,	0);

DROP TABLE IF EXISTS `master_gallery`;
CREATE TABLE `master_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '-',
  `image_path` varchar(100) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

TRUNCATE `master_gallery`;
INSERT INTO `master_gallery` (`id`, `title`, `image_path`, `type`) VALUES
(3,	'Sea World',	'assets/upload/gallery/Tempat-Wisata-Okinawa-Churaumi-Aquarium-di-Jepang.jpg',	0),
(4,	'Pasir Timbul',	'assets/upload/gallery/6-1-PasirTimbul-By-lutfi_ady.jpg',	0),
(5,	'Kebun Teh',	'assets/upload/gallery/Kebun-Teh-Pengalengan.jpg',	0),
(6,	'Situ Cileunca',	'assets/upload/gallery/Situ-Cileunca.jpg',	0),
(7,	'John Doe',	'assets/upload/gallery/9a3e855877a364071d4fd71b1e02b0c2.jpg',	1);

DROP TABLE IF EXISTS `master_home`;
CREATE TABLE `master_home` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `content_1` varchar(50) NOT NULL,
  `content_2` varchar(50) DEFAULT ' ',
  `content_3` text,
  `type` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=latin1;

TRUNCATE `master_home`;
INSERT INTO `master_home` (`id`, `content_1`, `content_2`, `content_3`, `type`) VALUES
(24,	'Let’s build something',	'beautiful together',	'',	0),
(25,	'We help startups & companies to create great',	'Web Interface, Brand & Web/iOS app design',	'',	1),
(26,	'This is what we do',	'A creative studio where art enthusiast and tech fr',	'connects over innovative Ideas.',	2),
(29,	'Our latest work',	'See our few selected work , our clients appreciate',	'a lot Nulla vitae elit libero, a pharetra augue. M',	3),
(31,	'Paket Wisata',	'',	'Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec id elit non mi porta gravida at eget metus. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Nulla vitae elit libero, a pharetra augue. Maecenas sed diam eget risus.\n',	5),
(32,	'Sewa Kendaraan',	'',	'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam porta sem malesuada magna mollis euismod. Praesent commodo cursus magna, vel scelerisque nisl Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Integer posuere erat a ante venenatis dapibus posuere velit aliquet.',	4);

DROP TABLE IF EXISTS `master_rent`;
CREATE TABLE `master_rent` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `price` int(11) NOT NULL DEFAULT '0',
  `vehicle_type` varchar(35) DEFAULT NULL,
  `vehicle_brand` varchar(25) DEFAULT NULL,
  `duration` varchar(25) NOT NULL DEFAULT '24 Jam',
  `region` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

TRUNCATE `master_rent`;
INSERT INTO `master_rent` (`id`, `price`, `vehicle_type`, `vehicle_brand`, `duration`, `region`) VALUES
(2,	300000,	'bike',	'Yamaha mio',	'2 days',	'Bandung - pasteur'),
(3,	450000,	'bike',	'Yamaha Nmax',	'2 Days',	'Bandung - Lembang'),
(5,	1000000,	'car',	'avanza',	'3 days',	'dipatiukur');

DROP TABLE IF EXISTS `master_testimony`;
CREATE TABLE `master_testimony` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `sub_content` varchar(100) DEFAULT '-',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

TRUNCATE `master_testimony`;
INSERT INTO `master_testimony` (`id`, `created_by`, `content`, `sub_content`) VALUES
(11,	'Albert Einstein',	'Insanity: doing the same thing over and over again and expecting different results.',	''),
(12,	'Albert Einstein',	'Logic will get you from A to B. Imagination will take you everywhere.',	''),
(13,	'Nikola Tesla',	'The scientists of today think deeply instead of clearly. ',	'One must be sane to think clearly, but one can think deeply and be quite insane.'),
(14,	'Nikola Tesla',	'The day science begins to study non-physical phenomena, it will make more progress in one',	'decade than in all the previous centuries of its existence.'),
(16,	'Bill Gates',	'Just in terms of allocation of time resources, religion is not very efficient. ',	'There\'s a lot more I could be doing on a Sunday morning.'),
(17,	'Bill Gates',	'Technology is just a tool. In terms of getting the kids working together and motivating them,',	' the teacher is the most important.'),
(18,	'Nikola Tesla',	'Our virtues and our failings are inseparable, like force and matter. ',	'When they separate, man is no more.');

DROP TABLE IF EXISTS `master_tour`;
CREATE TABLE `master_tour` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `price` int(11) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `image_path` varchar(100) DEFAULT NULL,
  `created_date` varchar(10) NOT NULL,
  `updated_date` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=latin1;

TRUNCATE `master_tour`;
INSERT INTO `master_tour` (`id`, `price`, `title`, `description`, `image_path`, `created_date`, `updated_date`) VALUES
(63,	1200000,	'Kebun Teh Pengalengan',	'Wisata Bandung satu ini pas untuk Teman Traveler yang ingin mencari suasana tenang, sembari duduk mengagumi panorama alam. Atmosfer yang ditawarkan di sekitar area Kebun Teh Pengalengan sungguh syahdu. Kualitas udaranya juga bagus, sangat segar dan membuat tubuh rileks.\n\nMengunjungi lokasi bakal terasa lebih berkesan jika bersama pasangan atau sosok terkasih. Namun datang sendirian pun tak ada salahnya, sebab keindahan alam akan selalu terasa mempesona meskipun hanya dinikmati seorang diri.',	'assets/upload/tour/Kebun-Teh-Pengalengan.jpg',	'03-10-2019',	NULL),
(67,	12000000,	'Hakone Tozan Train Line',	'Kota Hokane yang berada di Prefektur Kanagawa terkenal dengan sumber mata air panasnya. Tapi, Hakone Tozan Train Line yang ada di sana juga nggak kalah populer lho.\n\nDestinasi ini bakal dibanjiri traveler saat musim semi di bulan Juni. Pemandangan cantik jadi alasan mengapa mereka datang ke sana.',	'assets/upload/tour/19624958_140086233236094_5370433132757516288_n.jpg',	'04-10-2019',	NULL),
(68,	9000000,	'Golden Pavilion',	'Golden Pavilion adalah salah satu kuil Buddha paling populer di Jepang. Tempat wisata ini juga dikenal dengan nama Kinkakuji dan Rokuonji. Sebelumnya, kuil ini adalah tempat peristirahatan Ashikaga Yoshimitsu setelah masa pensiunnya. Namun sekarang ini kuil menjadi tempat wisata yang dikunjungi banyak wisatawan domestik dan asing setiap harinya.\n\nCiri dari Golden Pavilion adalah atapnya yang berwarna emas. Selain dapat melihat arsitektur kuno Jepang yang khas, Anda juga bisa menikmati teh panas di Tea Garden yang berada di kawasan wisata ini. di sini juga terdapat toko suvenir khas Golden Pavilion.',	'assets/upload/tour/Golden-Pavilion.jpg',	'04-10-2019',	NULL),
(69,	15000000,	'Gunung Fuji',	'Gunung Fuji tentunya sudah tak asing lagi bagi Anda. Gunung setinggi 3.776 meter ini merupakan gunung tertinggi dan terpopuler di Jepang. Gunung ini terletak di antara Yamanashi dan Shizuoka dan jika hari sedang cerah, Anda bisa melihatnya dari Tokyo dan Yokohama. Cara lainnya untuk menikmati keindahan Gunung Fuji adalah dengan naik kereta antara Tokyo dan Osaka.\n\nUntuk menikmati keindahan Gunung Fuji dari dekat, Anda bisa mendakinya. Terdapat lima danau indah di atas ketinggian sekitar 1.000 meter di atas permukaan laut. Kelima danau tersebut di antaranya adalah Yamanakako, Motosuko, Saiko, Shojiko dan Kawaguchiko.\n\nSelain itu, terdapat sebuah rest area, Fujigoko, yang menyediakan resort, camping ground, kolam pemancingan, museum dan juga pemandian air hangat.',	'assets/upload/tour/Liburan-ke-Gunung-Fuji-di-Jepang.jpg',	'04-10-2019',	NULL),
(70,	2500000,	'Raja Ampat - Wayag',	'Wayag juga termasuk salah satu ikonnya Raja Ampat. Jejeran batu karang berbalut warna hijau di tengah lautan biru yang menyajikan panorama luar biasa indah membuat Raja Ampat begitu mendunia.',	'assets/upload/tour/4-3-Wayag-By-tiarauliana.jpg',	'04-10-2019',	NULL);

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `username` varchar(40) NOT NULL,
  `password` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

TRUNCATE `user`;
INSERT INTO `user` (`username`, `password`) VALUES
('admin',	'827ccb0eea8a706c4c34a16891f84e7b');

-- 2019-10-03 22:17:11